package org.genesys2.server.model.impl;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.genesys2.server.model.VersionedModel;
import org.hibernate.annotations.Type;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@Entity
@Table(name = "georegion")
public class GeoRegion extends VersionedModel {

	private static final long serialVersionUID = -1L;

	@Column
	private String isoCode;

	@Column
	private String name;

	@ManyToOne()
	@JoinColumn(name = "parentId")
	private GeoRegion parentRegion;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "region")
	private List<Country> countries;

	@Lob
	@Type(type = "org.hibernate.type.TextType")
	private String nameL;

	@Transient
	private JsonNode nameJ;

	public String getIsoCode() {
		return isoCode;
	}

	public void setIsoCode(String isoCode) {
		this.isoCode = isoCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Country> getCountries() {
		return countries;
	}

	public void setCountries(List<Country> countries) {
		this.countries = countries;
	}

	public GeoRegion getParentRegion() {
		return parentRegion;
	}

	public void setParentRegion(GeoRegion parentRegion) {
		this.parentRegion = parentRegion;
	}

	public String getNameL() {
		return nameL;
	}

	public void setNameL(String nameL) {
		this.nameL = nameL;
	}

	public void setName(Locale locale, String name) {
		// TODO Fix this
		final ObjectMapper mapper = new ObjectMapper();
		try {
			this.nameJ = mapper.readTree(nameL);
			// ObjectNode newLang =
			// mapper.createObjectNode().put(locale.getLanguage(), name);
			// System.err.println(newLang);
		} catch (final IOException e) {
		}
	}

    public String getName(Locale locale) {
        return getNameLocal(locale);
    }

	private synchronized String getNameLocal(Locale locale) {
		if (this.nameJ == null && this.nameL != null) {
			final ObjectMapper mapper = new ObjectMapper();
			try {
				this.nameJ = mapper.readTree(nameL);
			} catch (final IOException e) {
				e.printStackTrace();
			}
		}

		return this.nameJ != null && this.nameJ.has(locale.getLanguage()) ? this.nameJ.get(locale.getLanguage()).textValue() : this.name;
	}
}