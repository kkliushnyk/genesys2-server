<!DOCTYPE html>

<%@include file="/WEB-INF/jsp/init.jsp" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<html>
<head>
    <title><spring:message code="logger.edit.page"/></title>
</head>

<body>
<h1><spring:message code="logger.edit.page"/></h1>

  <div class="row">
    <div class="col-sm-4"><label><spring:message code="logger.name" /></label></div>
    <div class="col-sm-8"><p class=".form-control-static">${logger.name}</p></div>
  </div>
  <div class="row">
    <div class="col-sm-4"><label><spring:message code="logger.log-level" /><label></div>
    <div class="col-sm-8 control-label"><p class=".form-control-static">${logger.level}</p></div>
  </div>
  <div class="row">
    <div class="col-sm-4"><label><spring:message code="logger.appenders" /><label></div>
    <div class="col-sm-8">
    	<c:forEach items="${appenders}" var="appender">
        	<p class=".form-control-static"><c:out value="${appender.name}" /></p>
    	</c:forEach>
    </div>
  </div>

    <form method="post" class="form-horizontal" action=<c:url value="/admin/logger/changeLoger"/> >
		<!-- CSRF protection -->
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
      <input type="hidden" name="loggerName" value="${logger.name}"/>

      <div class="form-group">
      	<label class="col-sm-4 control-label"><spring:message code="logger.log-level" /></label>
      	<div class="col-sm-8">
        <select class="form-control" name="loggerLevel">
          <c:forTokens items="all,debug,info,warn,error,fatal,off,trace" delims="," var="level">
            <option value="${level}" ${level == logger.level.toString().toLowerCase() ? "selected" : ""}>
                ${level}
            </option>
          </c:forTokens>
        </select>
        </div>
      </div>

      <div class="form-group">
      <div class="col-sm-offset-4 col-sm-8">
        <input type="submit" class="btn btn-group-sm" value="<spring:message code="save" />">
        <a class="btn btn-default" href="<c:url value="/admin/logger/" />"><spring:message code="cancel" /></a>
      </div>
      </div>

    </form>
</div>
</body>
</html>
