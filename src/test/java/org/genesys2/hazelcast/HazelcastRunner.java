package org.genesys2.hazelcast;

import java.io.IOException;

import org.genesys2.spring.config.SpringProperties;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;

import com.hazelcast.config.Config;
import com.hazelcast.config.GroupConfig;
import com.hazelcast.config.JoinConfig;
import com.hazelcast.config.NetworkConfig;
import com.hazelcast.config.TcpIpConfig;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.ManagedContext;
import com.hazelcast.spring.context.SpringManagedContext;

public class HazelcastRunner {

	@Configuration
	@ActiveProfiles("dev")
	@Import({ SpringProperties.class })
	public static class AppConfig {
		protected int hazelPort=5702;
		@Value("${hazelcast.instanceName}")
		protected String instanceName = "genesys";
		@Value("${hazelcast.password}")
		protected String password;
		@Value("${hazelcast.name}")
		protected String name;

		@Bean
		public Long number() {
			return new Long(13);
		}

		@Bean
		public ManagedContext managedContext() {
			return new SpringManagedContext();
		}

		@Bean
		public HazelcastInstance hazelcast(ManagedContext managedContext) {
			Config cfg = new Config();
			cfg.setManagedContext(managedContext);
			cfg.setInstanceName(instanceName);

			GroupConfig groupConfig = cfg.getGroupConfig();
			groupConfig.setName(name);
			groupConfig.setPassword(password);

			cfg.setProperty("hazelcast.merge.first.run.delay.seconds", "5");
			cfg.setProperty("hazelcast.merge.next.run.delay.seconds", "5");
			cfg.setProperty("hazelcast.logging.type", "log4j");
			cfg.setProperty("hazelcast.icmp.enabled", "true");

			NetworkConfig network = cfg.getNetworkConfig();
			network.setPort(hazelPort);
			network.setPortAutoIncrement(true);

			JoinConfig join = network.getJoin();
			join.getMulticastConfig().setEnabled(false);
			TcpIpConfig tcpIpConfig = join.getTcpIpConfig();
			tcpIpConfig.setEnabled(true);
			tcpIpConfig.setConnectionTimeoutSeconds(20);
			// See if there's a local HZ running
			tcpIpConfig.addMember("127.0.0.1:5701");

			HazelcastInstance instance = Hazelcast.newHazelcastInstance(cfg);
			return instance;
		}
	}

	public static void main(String[] args) {
		AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(AppConfig.class);

		HazelcastInstance instance = ctx.getBean(HazelcastInstance.class);
		System.out.println(instance);
		System.out.println("Press any key to quit.");
		try {
			System.in.read();
		} catch (IOException e) {
		}

		ctx.close();
	}
}
